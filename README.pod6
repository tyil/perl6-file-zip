=begin pod

=NAME    File::Zip
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.1.2

=head1 Description

A wrapper around zip files, using zip/unzip commands

=head1 Installation

Install this module through L<zef|https://github.com/ugexe/zef>:

=begin code :lang<sh>
zef install File::Zip
=end code

=head1 License

This module is distributed under the terms of the AGPL-3.0.

=end pod
