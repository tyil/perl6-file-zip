#! /usr/bin/env perl6

use v6.c;

use Test;
use File::Zip;
use File::Temp;

plan 3;

subtest "Instantiate existing zip", {
	plan 4;

	my IO::Path $file .= new: "t/files/google.com!zaaksysteem.nl!1538265600!1538351999.zip";
	my File::Zip $zip .= new: $file;

	ok $zip.path, "Path to zip file is accessible";
	ok $zip.path.e, "Zip file exists";

	subtest "List files in zip", {
		plan 3;

		my %files = $zip.files;

		is %files.elems, 1, "Correct number of files found in zip";
		ok %files<google.com!zaaksysteem.nl!1538265600!1538351999.xml>:exists, "Expected file found";
		is %files<google.com!zaaksysteem.nl!1538265600!1538351999.xml><length>, 2202, "Length is correct";
	}

	subtest "Extract files", {
		plan 2;

		my $tempdir = tempdir;

		ok $zip.extract($tempdir), ".extract reports success";
		ok $tempdir.IO.add("google.com!zaaksysteem.nl!1538265600!1538351999.xml").e, "File was correctly extracted";
	}
}

subtest "Instantiate existing, non-zip file", {
	plan 1;

	my IO::Path $file .= new: "t/files/not-a.zip";

	dies-ok { my File::Zip $zip .= new: $file; }, "Dies when trying to load a file which is not a zip";
}

subtest "Instantiate non-existing zip", {
	plan 1;

	my IO::Path $file .= new: "t/files/dev-null";

	dies-ok { my File::Zip $zip .= new: $file; }, "Dies when trying to load a non-existing file";
}

# vim: ft=perl6 noet
