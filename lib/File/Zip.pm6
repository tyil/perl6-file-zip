#! /usr/bin/env false

use v6.c;

use File::Which;

unit class File::Zip;

#| The path to the zip file that's being wrapped around.
has IO::Path $.path;

#| The %!cache property holds some cached information on the zip.
has %!cache;

#| Extract the files from the zip file into a directory given as a C<IO::Path>.
multi method extract (
	IO::Path:D $destination,
	--> Bool
) {
	my Proc $unzip = run « unzip -d "$destination.absolute()" "$!path.absolute()" », :out;

	$unzip.exitcode == 0;
}

#| Extract the files from the zip file into a directory path.
multi method extract (
	Str:D $destination,
) {
	samewith($destination.IO);
}

#| Extract the files from the zip into the currect directory.
multi method extract (
) {
	samewith($*CWD);
}

#| Retrieve the list of files in the zip archive as a Hash, with the keys
#| corresponding to the filenames in the archive.
method files (
	Bool:D :$cache = True,
	--> Hash
) {
	return %!cache<files> if $cache && %!cache<files>;

	my Proc $unzip = run « unzip -l "$!path.absolute()" », :out, :err;
	my @lines = $unzip
		.out
		.lines
		.skip(3)
		.head(*-2)
		;
	my %files;

	for @lines {
		my ($length, $date, $time, $name) = $_.words;

		%files{$name} = {
			length => $length.Int,
			:$date,
			:$time,
		}
	}

	%!cache<files> = %files if $cache;

	%files;
}

#| Wrap a file path into a File::Zip object. This allows for easy interaction
#| with a zip archive.
multi method new (
	IO::Path:D $path,
) {
	die "File does not exist" unless $path.e;
	die "Given path is a directory" if $path.d;
	die "You don't have the zip utility available in your \$PATH" unless which("zip");
	die "You don't have the unzip utility available in your \$PATH" unless which("unzip");

	my Proc $file = run « file "$path.absolute()" », :out;

	die "$path.absolute() is not a zip archive" unless $file.out.slurp ~~ m:i/"zip archive data,"/;

	self.bless(:$path);
}

multi method new (
	Str:D $path,
) {
	samewith($path.IO);
}

=begin pod

=NAME    File::Zip
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.1.2

=head1 Description

C<File::Zip> is a wrapper around the C<zip> and C<unzip> utitlities. This
module allows you to interact with zip files using a Perl 6 interface.

=head1 Examples

=begin code
use File::Zip;

my File::Zip $zip .= new: "/tmp/some.zip";

# List the files contained in the zip file
$zip.files.map(*.say);

# Extract the zip into a given directory
$zip.extract("/tmp");
=end code

=end pod

# vim: ft=perl6 noet
